
/**
 * Class for saving position of a feature
 * @author 
 *
 */
public class Position {
	int row;
	int column;
	String type;
	
	public Position(int rowX, int columnY) {
		this.row = rowX;
		this.column = columnY;
		this.type = null;
	}
	
	public boolean isEqual(Position pos) {
		if (pos.getType().equals(this.type) && this.row == pos.getRow() && this.column == pos.getColumn()) {
			return true;
		}
		return false;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}
}
