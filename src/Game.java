import javax.swing.JOptionPane;

import java.util.Queue;
import java.util.LinkedList;

/**
 * Game class that includes main function to start and manage game system
 * @author Moosuk Pyun, Bohan Yu, Lachlan Kerr and Tom Castagnone
 *
 */
public class Game{
	private final int GAMETYPE_NONE = -1;
	private final int GAMETYPE_SINGLE = 0;
	private final int GAMETYPE_MULTI = 1;
	private final int GAMETYPE_AI = 2;
	
	private final int GAMETYPE_SINGLE_RUN = -3;
	private final int GAMETYPE_MULTI_RUN = -2;
	private final int GAMETYPE_AI_RUN = -4;
	private final int GAMETYPE_NONE_RUN = -5;
	
	GameBoard board;
	boolean running;
	Queue<GameBoard> maps;
	FrontEnd gui;
	private int round;
	private int player1Score;
	private int player2Score;
	
	public Game (FrontEnd gui) {
		this.maps = new LinkedList<GameBoard>();
		this.gui = gui;
		running = true;
	}
	
	/**
	 * Method to add maps
	 * @param board
	 */
	public void addMap(GameBoard board) {
		this.maps.add(board);
	}
	
	/**
	 * This method controls the game, the game runs while this method is on.
	 */
	public void start() {
		boolean exit = false;
		
		while (!exit) {
			System.out.println("");
			Queue<GameBoard> copyMaps = new LinkedList<GameBoard>();
			GameBoard initBoard = new GameBoard(10, 10);

			
			int [][] predefined2 = {
					{4,4,4,4,4,4,4,4,4,4},
					{4,4,0,0,0,0,0,0,4,4},
					{4,4,0,0,0,2,0,0,4,4},
					{4,4,4,4,4,4,0,0,4,4},
					{4,4,0,0,4,4,2,0,4,4},
					{4,4,0,0,0,0,0,0,4,4},
					{4,4,0,0,3,3,0,0,4,4},
					{4,4,4,0,0,0,4,0,4,4},
					{4,4,4,4,4,4,4,4,4,4},
					{4,4,4,4,4,4,4,4,4,4}

			};
			initBoard.setBoard(predefined2);
			initBoard.setStartingLocation(2,2);
			copyMaps.add(initBoard);
			this.round = 0;
			this.player1Score = 0;
			this.player2Score = 0;
		
			while ((!copyMaps.isEmpty() && gui.getGameType() != GAMETYPE_NONE_RUN)){
				running = true;
				this.round++;


				
				//map theMap = new map();
				//int[][] board = theMap.newMap();
				
				//map generation
				RandomMap mapGen = new RandomMap();;
					
				GameBoard newBoard = new GameBoard(mapGen.getTotalSpace(), mapGen.getTotalSpace());
				int[][] temp = null;
				while (temp == null)
					temp = mapGen.newMap();
				newBoard.setBoard(temp);
				newBoard.setStartingLocation(mapGen.getPosition().getRow(), mapGen.getPosition().getColumn());
				System.out.println("board");
				newBoard.printBoard();
				copyMaps.add(newBoard);
	
				GameBoard board = copyMaps.poll();
				int[][] aiMatrix = board.getOriginalBoard();
				
				Position aiPos = board.getCloneChar();
				GameBoard aiBoard = new GameBoard(board.getRow(), board.getColumn());
				aiBoard.setBoard(aiMatrix);
				aiBoard.setStartingLocation(aiPos.getRow(), aiPos.getColumn());
				
				AI ai = new AI(aiBoard);
				Thread aiThread = new Thread(ai);
				aiThread.start();
				board.printBoard();
				
				//initialise timer
				long startTime = System.currentTimeMillis();;
				long endTime = 100 - 10*this.round;
				if (endTime < 10) {
                    endTime = 10;
				}
				Timer time = new Timer(gui);
				
				if (gui.getGameType() != GAMETYPE_NONE) {
					gui.setNumCols(board.getColumn());
					gui.setNumRows(board.getRow());
				}
				gui.setBoard(board);
				gui.placeImagesFromBoard(board.getBoard());
				
				board.storeNumberOfGoals();
				GameBoard multiBoard = board;
				Queue<String> path = null;
				while(running) {
					if (gui.getSkipState() == true) {
						gui.setSkipState(false);
						aiThread.interrupt();
						break;
					}
					
					if (ai.isFound() == true) {
						path = ai.getStrPath();
						ai.goalState = null;
					} else if(path != null && gui.getGameType() == GAMETYPE_AI_RUN) {
						if (time.isTimeChanged(startTime, endTime)) {
							String move = path.poll();
							if (move.equals("left")){
								multiBoard.moveLeftPlayer2();
								gui.setPlayerImage2("left");
							} else if (move.equals("right")) {
								multiBoard.moveRightPlayer2();
								gui.setPlayerImage2("right");
							} else if (move.equals("up")) {
								multiBoard.moveUpPlayer2();
								gui.setPlayerImage2("up");
							} else if (move.equals("down")) {
								multiBoard.moveDownPlayer2();
								gui.setPlayerImage2("down");
							}
							gui.placeImagesFromBoard(multiBoard.getBoard());
	
	
						}
					}

			        gui.setRound(round);
			        if (gui.getGameType() != GAMETYPE_SINGLE_RUN)
			        	gui.setScore(player1Score, player2Score);
			        else
			        	gui.setScore(-1, -1);
					
					if (gui.getGameType() == GAMETYPE_AI) {
						startTime = System.currentTimeMillis();
						gui.setNumCols(board.getColumn()*2);
						MultiPlayer multi = new MultiPlayer();
						multiBoard = multi.MutiPlayerBoard(board);
						gui.setGameType(GAMETYPE_AI_RUN);
						gui.setBoard(multiBoard);
						gui.placeImagesFromBoard(multiBoard.getBoard());
						multiBoard.storeNumberOfGoals();
					}
					
					if (gui.getGameType() == GAMETYPE_MULTI) {
						startTime = System.currentTimeMillis();
						gui.setNumCols(board.getColumn()*2);
						MultiPlayer multi = new MultiPlayer();
						multiBoard = multi.MutiPlayerBoard(board);
						gui.setGameType(GAMETYPE_MULTI_RUN);
						gui.setBoard(multiBoard);
						gui.placeImagesFromBoard(multiBoard.getBoard());
						multiBoard.storeNumberOfGoals();
					}
					else if (gui.getGameType() == GAMETYPE_SINGLE) {
						startTime = System.currentTimeMillis();
						gui.setNumCols(board.getColumn());
						gui.setGameType(GAMETYPE_SINGLE_RUN);
						gui.setBoard(board);
						gui.placeImagesFromBoard(board.getBoard());
						board.storeNumberOfGoals();
					}
					if (gui.getGameType() != GAMETYPE_NONE) {//otherwise counts down on main menu 
						time.TimeDown(startTime, endTime);
					} 
					gui.setTimer(time.getTime());
					if (board.checkFinish() || multiBoard.checkFinishMulti()) {
						
						this.running = false;
						if (gui.getGameType() == GAMETYPE_SINGLE_RUN)
							JOptionPane.showMessageDialog(null, "You have won! :D", "Success!", JOptionPane.INFORMATION_MESSAGE);
						else if (gui.getGameType() == GAMETYPE_MULTI_RUN || gui.getGameType() == GAMETYPE_AI_RUN) {
							JOptionPane.showMessageDialog(null, multiBoard.getResult() + " has won round "+this.round+ " :D", "Success!", JOptionPane.INFORMATION_MESSAGE);
							if (multiBoard.getResult().equals("Player1")) {
								this.player1Score++;
							} else if (multiBoard.getResult().equals("Player2")) {
								this.player2Score++;
							}
							if (this.player2Score == 5) {
								JOptionPane.showMessageDialog(null, "Player2 is the WINNER", null, JOptionPane.INFORMATION_MESSAGE);
								gui.backButtonPressed();

							} else if (this.player1Score == 5) {
								JOptionPane.showMessageDialog(null, "Player1 is the WINNER", null, JOptionPane.INFORMATION_MESSAGE);
								gui.backButtonPressed();
							}
						}
						
						aiThread.interrupt();
						break;
					}
					if (gui.getGameType() == GAMETYPE_NONE_RUN) {
						aiThread.interrupt();
						break;
					}
				}
				if (gui.getGameType() == GAMETYPE_AI_RUN) //ai
					gui.setGameType(GAMETYPE_AI);
				else if (gui.getGameType() == GAMETYPE_MULTI_RUN) //multi
					gui.setGameType(GAMETYPE_MULTI);
				else if (gui.getGameType() == GAMETYPE_SINGLE_RUN)//single
					gui.setGameType(GAMETYPE_SINGLE);
			}
		}
		System.exit(0);
		
	}
	
	/**
	 * Get the current GameBoard
	 * @return GameBoard board
	 */
	public GameBoard getBoard(){
		return this.board;
	}
	
	public static void main(String args[]) {
		FrontEnd gui = new FrontEnd(10, 10);
		gui.setVisible(true);
		
		int rowSize = 10;
		int columnSize = 10;
		Game game = new Game(gui);
		GameBoard newBoard2 = new GameBoard(rowSize, columnSize);

	
		int [][] predefined2 = {
				{4,4,4,4,4,4,4,4,4,4},
				{4,4,0,0,0,0,0,0,4,4},
				{4,4,0,0,0,2,0,0,4,4},
				{4,4,4,4,4,4,0,0,4,4},
				{4,4,0,0,4,4,2,0,4,4},
				{4,4,0,0,0,0,0,0,4,4},
				{4,4,0,0,3,3,0,0,4,4},
				{4,4,4,0,0,0,4,0,4,4},
				{4,4,4,4,4,4,4,4,4,4},
				{4,4,4,4,4,4,4,4,4,4}

		};
		newBoard2.setBoard(predefined2);
		newBoard2.setStartingLocation(2,2);
		
		//newBoard.setStartingLocation(2, 2);
		//game.addMap(newBoard2);
		game.addMap(newBoard2);
		
		game.start();
	}
	
}
