import java.util.ArrayList;

/**
 * Strategy for AI
 * @author 
 *
 */
public interface AIStrategy {
	public int getH(Position character, ArrayList<Position> featurePos, int numMove);
}
