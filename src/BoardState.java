import java.util.ArrayList;
import java.util.Iterator;

/**
 * Board State is a class to save board state that will be used in A* search of AI
 * @author 
 *
 */
public class BoardState implements Comparable<BoardState>{
	BoardState parent;
	Position character;
	ArrayList<Position> featurePosition;
	int numBox;
	boolean finish;
	int numMove;
	int hValue;
	String movement;
	
	public BoardState(Position charPos, ArrayList<Position> feature, int num) {
		this.parent = null;
		this.movement = null;
		this.character  = charPos;
		this.featurePosition = feature;
		this.numMove = num;
		this.finish = this.checkFinish();
		this.hValue = 0;
		this.setNumBox();
		
	}
	
	/**
	 * Getter for getting number of moves
	 * @return number of moves
	 */
	public int getNumMove() {
		return numMove;
	}
	
	/**
	 * Setter for setting number of moves
	 * @param numMove
	 */
	public void setNumMove(int numMove) {
		this.numMove = numMove;
	}

	/**
	 * Setter for setting number of box
	 */
	public void setNumBox() {
		int num = 0;
		Iterator<Position> itr = featurePosition.iterator();
		while(itr.hasNext()) {
			Position f = (Position) itr.next();
			if (f.getType().equals("box")) {
				num++;
			}
		}
		this.numBox = num;
	}
	
	/**
	 * Getter for getting number of boxes
	 * @return
	 */
	public int getNumBox() {
		return this.numBox;
	}
	
	/**
	 * Method to check if this board state is finish of game
	 * @return true if finished
	 * 		   false if not finished
	 */
	public boolean checkFinish() {
		Iterator<Position> itr = featurePosition.iterator();
		while(itr.hasNext()) {
			Position f = (Position) itr.next();
			if (f.getType().equals("goal") || f.getType().equals("goalPerson")) {
				return false;
			}
			
		}
		return true;
	}
	
	/**
	 * Override of compareTo method to use in priority queue
	 */
	@Override
	public int compareTo(BoardState ss) {
		return Integer.valueOf(hValue).compareTo(Integer.valueOf(ss.getHValue()));
	}
	
	/**
	 * Checks if two board state is equal, its equal if all the features are identical
	 * @param bs
	 * @return true if identical
	 * 		   false if not identical
	 */
	public boolean isEqual(BoardState bs) {
		int numCorrect = 0;
		ArrayList<Position> bsFeature = bs.getFeaturePosition();
		if (this.numBox != bs.getNumBox()) {
			return false;
		} else if (this.character.isEqual(bs.getCharacter())) {
			numCorrect++;
		} else {
			return false;
		}
		
		Iterator<Position> itr = featurePosition.iterator();
		while(itr.hasNext()) {
			Position f = (Position) itr.next();
			if (f.getType().equals("box")){
				Iterator<Position> bsItr = bsFeature.iterator();
				while(bsItr.hasNext()) {
					Position bsP = bsItr.next();
					if (bsP.isEqual(f)) {
						numCorrect++;
					}
				}
				
			}
		}
		//System.out.print(Integer.toString(numCorrect));
		if (numCorrect == numBox + 1) {
			return true;
		}
		return false;
	}
	
	/**
	 * Getter for getting finish
	 * @return boolean finish
	 */
	public boolean getFinish() {
		return finish;
	}
	
	/**
	 * Getter for getting heuristic value
	 * @return int hValue
	 */
	public int getHValue(){
		return hValue;
	}
	
	/**
	 * Setter for setting heuristic value
	 * @param value
	 */
	public void setHValue(int value){
		this.hValue = value;
	}
	
	/**
	 * Getter for getting parent board state
	 * @return BoardState parent
	 */
	public BoardState getParent() {
		return parent;
	}
	
	/**
	 * Setter for setting parent BoardState
	 * @param parent
	 */
	public void setParent(BoardState parent) {
		this.parent = parent;
	}
	
	/**
	 * Getter for getting the position of character
	 * @return Position character
	 */
	public Position getCharacter() {
		return character;
	}
	
	/**
	 * Setter for setting character
	 * @param character
	 */
	public void setCharacter(Position character) {
		Position charac = new Position(character.getRow(), character.getColumn());
		charac.setType("character");
		this.character = charac;
	}
	
	/**
	 * Getter for getting movement
	 * @return String movement
	 */
	public String getMovement() {
		return movement;
	}
	
	/**
	 * Setter for setting movement
	 * @param movement
	 */
	public void setMovement(String movement) {
		this.movement = movement;
	}
	
	/**
	 * Getter for returning arraylist of feature positions
	 * @return ArrayList<Position> featurePosition
	 */
	public ArrayList<Position> getFeaturePosition() {
		return featurePosition;
	}
	/**
	 * Setter for setting feature positions
	 * @param featurePosition
	 */
	public void setFeaturePosition(ArrayList<Position> featurePosition) {
		this.featurePosition = featurePosition;
	}

}
