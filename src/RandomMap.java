/**
 * This function generates random maps which can be used in the game.
 */

import java.util.Random;

public class RandomMap {
	static int FREESPACE = 0;
	static int WALL = 4;
	static int CRATE = 2;
	static int FINISH = 3;
	static int PLAYER = 1;
	
	int totalSpace = 10 + check(3);
	Position playerPosition;
	
	
	int totalSize = totalSpace*totalSpace;
	int[][] totalBoard = new int[totalSpace][totalSpace];

		
	public int[][] newMap(){
		
		/**
		 * The map is a 2D array. Initially, everything in the map is a wall
		 */
		for(int x = 0; x < totalSpace; x++){
			for(int y = 0; y < totalSpace; y++){
				totalBoard[x][y] = WALL;	
			}
			
		}
		
		/**
		 * This method starts in the middle of the board and branches outwards creating free spaces on the map.
		 * The method stops before it reaches the outer border of the map.
		 */
		int mid = totalSpace/2;
		totalBoard[mid][mid] = FREESPACE;
		for(int i = 0; i < totalSpace; i++){
			int x = mid, y = mid;
			for(int j = 0; j < 2*totalSpace; j++){
				int dir = check(4);
				if(dir == 0){
					if(x + 1 != totalSpace - 1){
						totalBoard[x++][y] = FREESPACE;	
					}
				} else if(dir == 1){
					if(y + 1 != totalSpace - 1){
						totalBoard[x][y++] = FREESPACE;
					}
				} else if(dir == 2){
					if(x - 1 != 0){
						totalBoard[x--][y] = FREESPACE;
					}
				} else if(dir == 3){
					if(y - 1 != 0){
						totalBoard[x][y--] = FREESPACE;
					}
				}				
			}
		}
		
		/**
		 * random walls are placed throughout the remainder of the board but are placed in specific places, not so to make the map unplayable.
		 */
		int numWalls = totalSpace/2;
		int counterC = 0;
		while(numWalls > 0){
			int x = check(totalSpace), y = check(totalSpace);
			if(checkLegal(x,y) == 1){
				totalBoard[x][y] = WALL;
				numWalls--;
			}
			counterC++;
			if(counterC > 1000){
				return null;
			}
		}
		
		/**
		 * now crates are added to the board. A separate method is used to ensure that the crates aren't placed in impossible places (ie like in corners).
		 */
		int numCrates = 0;
		int counterB = 0;
		int c = 3 + check(3);
		while(numCrates < c){
			int x = check(totalSpace), y = check(totalSpace);
			if(checkLegal(x,y) == 1){
				totalBoard[x][y] = CRATE;
				numCrates++;
			}
			counterB++;
			if(counterB > 1000){
				return null;
			}
		}
		
		/**
		 * finish line places are placed on the board similar to how the crates are placed
		 */
		int counter = 0;
		while(numCrates > 0){
			int x = check(totalSpace), y = check(totalSpace);
			if(finLegal(x,y) == 1){
				totalBoard[x][y] = FINISH;
				numCrates--;
			}
			counter++;
			if(counter > 1000){
				return null;
			}
		}
		
		/**
		 * the player is placed on the board similar to how the crates are placed
		 */
		boolean player = false;
		int counterD = 0;
		while(player == false){
			int x = check(totalSpace), y = check(totalSpace);
			if(finLegal(x,y) == 1){
				totalBoard[x][y] = PLAYER;
				player = true;
			}
			counterD++;
			if(counterD > 1000){
				return null;
			}
		}		
		for(int i = 0; i < totalSpace; i++) {
			for (int j = 0; j < totalSpace; j++) {
				if (totalBoard[i][j] == PLAYER) {
					System.out.println("Player found");
					playerPosition = new Position(i,j);
				}
			}
		}
		
		return totalBoard;
			
	}
	
	
	/**
	 * 
	 * This method prints out the map
	 */
	public void print2d(int[][] a){
		for(int i = 0; i < totalSpace; i++){
			if(i < 10){
				System.out.print(" "+i+") ");
			} else {
				System.out.print(i+") ");
			}
				

			for(int j = 0; j < totalSpace; j++){
				if(a[i][j] == WALL){
					System.out.print("= ");
					
				} else if(a[i][j] == CRATE){
					System.out.print("0 ");
					
				} else if(a[i][j] == FINISH){
					System.out.print("X ");
					
				} else if(a[i][j] == PLAYER){
					System.out.print("G ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		
		}
		System.out.println();
		for(int j = 0; j < totalSpace; j++){
			System.out.print("  ");
		}
		System.out.println("      "+totalSpace);
	}
	
	/**
	 * 
	 * because the maps are randomly generated, a random number generator is used.
	 */
	public int check(int a){
		Random checkR = new Random();
		int check = checkR.nextInt(a);
		return check;
	}

	/**
	 * This method ensures that the current position is legal for a crate/player/finish line to be placed.
	 * essentially, if there is nothing around the position then it is called a legal position and an illegal position otherwise.
	 */
	public int checkLegal(int x, int y){
		
		if(totalBoard[x][y] != FREESPACE){
			return 0;
		}
		
		if(totalBoard[x-1][y-1] == FREESPACE && totalBoard[x][y-1] == FREESPACE && totalBoard[x+1][y-1] == FREESPACE){
			if(totalBoard[x-1][y] == FREESPACE && totalBoard[x+1][y] == FREESPACE){
				if(totalBoard[x-1][y+1] == FREESPACE && totalBoard[x][y+1] == FREESPACE && totalBoard[x+1][y+1] == FREESPACE){
					return 1;
				}
			}
		}
		
		return 0;
		
	}
	
	/**
	 * this method is very similar to the checkLegal method only not so strict.
	 */
	public int finLegal(int x, int y){
		
		if(totalBoard[x][y] != FREESPACE){
			return 0;
		}
		
		if(totalBoard[x][y-1] == CRATE || totalBoard[x+1][y] == CRATE || totalBoard[x][y+1] == CRATE || totalBoard[x-1][y] == CRATE){
			return 0;
		}
		
		if(x+2 < totalSpace && y + 2 < totalSpace){
			if(totalBoard[x+1][y] != WALL && totalBoard[x+2][y] != WALL){
				if(totalBoard[x][y+1] != WALL && totalBoard[x+1][y+1] != WALL && totalBoard[x+2][y+1] != WALL){
					if(totalBoard[x][y+2] != WALL && totalBoard[x+1][y+2] != WALL && totalBoard[x+2][y+2] != WALL){
						return 1;						
					}
				}
			}
		}
		
		if(x+1 < totalSpace && y + 2 < totalSpace && x-1 > 0){
			if(totalBoard[x-1][y] != WALL && totalBoard[x+1][y] != WALL){
				if(totalBoard[x-1][y+1] != WALL && totalBoard[x][y+1] != WALL && totalBoard[x+1][y+1] != WALL){
					if(totalBoard[x-1][y+2] != WALL && totalBoard[x][y+2] != WALL && totalBoard[x+1][y+2] != WALL){
						return 1;						
					}
				}
			}
		}
		
		if(x-2 > 0 && y + 2 < totalSpace){
			if(totalBoard[x-2][y] != WALL && totalBoard[x-1][y] != WALL){
				if(totalBoard[x-2][y+1] != WALL && totalBoard[x-1][y+1] != WALL && totalBoard[x][y+1] != WALL){
					if(totalBoard[x-2][y+2] != WALL && totalBoard[x-1][y+2] != WALL && totalBoard[x][y+2] != WALL){
						return 1;						
					}
				}
			}
		}
		
		if(x + 2 < totalSpace && y + 1 < totalSpace && y - 1 > 0){
			if(totalBoard[x+1][y] != WALL && totalBoard[x+2][y] != WALL){
				if(totalBoard[x][y-1] != WALL && totalBoard[x+1][y-1] != WALL && totalBoard[x+2][y-1] != WALL){
					if(totalBoard[x][y+1] != WALL && totalBoard[x+1][y+1] != WALL && totalBoard[x+2][y+1] != WALL){
						return 1;
					}
				}
			}
		}
				
		if(x+1 < totalSpace && y + 1 < totalSpace && x-1 > 0 && y - 1 > 0){
			if(totalBoard[x-1][y] != WALL && totalBoard[x+1][y] != WALL){
				if(totalBoard[x-1][y-1] != WALL && totalBoard[x][y-1] != WALL && totalBoard[x+1][y-1] != WALL){
					if(totalBoard[x-2][y+1] != WALL && totalBoard[x][y+1] != WALL && totalBoard[x+1][y+1] != WALL){
						return 1;
					}
				}
			}
		}
		
		if(x - 2 > 0 && y - 1 > 0 && y + 1 < totalSpace){
			if(totalBoard[x-2][y] != WALL && totalBoard[x-1][y] != WALL){
				if(totalBoard[x-2][y-1] != WALL && totalBoard[x-1][y-1] != WALL && totalBoard[x][y-1] != WALL){
					if(totalBoard[x-2][y+1] != WALL && totalBoard[x-1][y+1] != WALL && totalBoard[x][y+1] != WALL){
						return 1;
					}
				}
			}
		}	

		if(x+2 < totalSpace && y-2 > 0){
			if(totalBoard[x+1][y] != WALL && totalBoard[x+2][y] != WALL){
				if(totalBoard[x][y-2] != WALL && totalBoard[x+1][y-2] != WALL && totalBoard[x+2][y-2] != WALL){
					if(totalBoard[x][y-1] != WALL && totalBoard[x+1][y-1] != WALL && totalBoard[x+2][y-1] != WALL){
						return 1;
					}
				}
			}
		}
		
		if(x-1 > 0 && x + 1 < totalSpace && y - 2 > 0){
			if(totalBoard[x+1][y] != WALL && totalBoard[x-1][y] != WALL){
				if(totalBoard[x-1][y-2] != WALL && totalBoard[x][y-2] != WALL && totalBoard[x+1][y-2] != WALL){
					if(totalBoard[x-1][y-1] != WALL && totalBoard[x][y-1] != WALL && totalBoard[x+1][y-1] != WALL){
						return 1;
					}
				}
			}
		}

		if(x-2 > 0 && y - 2 > 0){
			if(totalBoard[x-1][y] != WALL && totalBoard[x-2][y] != WALL){
				if(totalBoard[x-2][y-2] != WALL && totalBoard[x-1][y-2] != WALL && totalBoard[x][y-2] != WALL){
					if(totalBoard[x-2][y-1] != WALL && totalBoard[x-1][y-1] != WALL && totalBoard[x][y-1] != WALL){
						return 1;
					}
				}
			}
		}
		
		return 0;

	}
	
	
	public int getTotalSpace() {
		return totalSpace;
	}
	
	public Position getPosition() {
		
		return playerPosition;
	}
}

