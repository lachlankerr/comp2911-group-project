import java.util.ArrayList;
import java.util.Stack;
import java.util.Iterator;
import java.util.Queue;
import java.util.PriorityQueue;
import java.util.LinkedList;

/**
 * AI class for the game, it implements a* search on board state 
 * @author
 *
 */
public class AI implements Runnable{
	GameBoard board;
	BoardState start;
	BoardState goalState;
	int numGoals;
	
	public AI(GameBoard inputBoard){
		this.board = inputBoard;
		this.goalState = null;
		this.start = new BoardState(this.board.getCharacter(), board.getFeaturePosition(), 0);
		
	}
	
	/**
	 * Checks if the AI found a path
	 * @return true if found
	 * 		   false if not found
	 */
	public boolean isFound() {
		boolean found = true;
		if (this.goalState == null) {
			found = false;
		}
		return found;
	}
	
	/**
	 * Returns Array of BoardStates that is in the path
	 * @return ArrayList<BoardState> path
	 */
	public ArrayList<BoardState> getPath() {
		ArrayList<BoardState> path = new ArrayList<BoardState>();
		Stack<BoardState> pathStack= new Stack<BoardState>();
		BoardState currNode = goalState;
		while (currNode.getParent() != null) {
			pathStack.push(currNode);
			currNode = currNode.getParent();
		}
		pathStack.push(currNode);
		
		while (!pathStack.empty()) {
			path.add(pathStack.pop());
		}
		
		return path;

	}
	
	/**
	 * Getter for getting string path in Queue
	 * @return Queue<String> path
	 */
	public Queue<String> getStrPath() {
		Queue<String> path = new LinkedList<String>();
		ArrayList<BoardState> bsPath = this.getPath();
		Iterator<BoardState> itr = bsPath.iterator();
		while (itr.hasNext()) {
			BoardState bs = itr.next();
			if (bs.getMovement() != null) {
				String move = bs.getMovement();
				System.out.print(move + " ");
				path.add(move);
			}
		}
		System.out.println("");
		return path;
	}
	
	/**
	 * Return the end state
	 * @return BoardState goalState
	 */
	public BoardState getEndState() {
		return goalState;
	}
	
	/**
	 * For running A* search on boardstates 
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		AIStrategy strat = new NearestDistance();
		PriorityQueue<BoardState> open = new PriorityQueue<BoardState>();
		ArrayList<BoardState> close = new ArrayList<BoardState>();
		int startHValue = strat.getH(start.getCharacter(), start.getFeaturePosition(), start.getNumMove());
		start.setHValue(startHValue);
		//System.out.println(Integer.toString(startHValue));
		this.board.clearBoard();
		//this.board.printBoard();
		this.board.setFeature(start.getFeaturePosition());
		this.board.setStartingLocation(start.getCharacter().getRow(), start.getCharacter().getColumn());
		//this.board.printBoard();


		open.add(start);
		
		while (open.size() > 0) {
			System.out.println("AI THINKING");
			BoardState current = open.poll();
			Position initPos = current.getCharacter();
			ArrayList<Position> initFeat = current.getFeaturePosition();
			int charRow = current.getCharacter().getRow();
			int charCol = current.getCharacter().getColumn();
			close.add(current);
			// Change the setting of board
			this.board.clearBoard();
			this.board.setStartingLocation(initPos.getRow(), initPos.getColumn());
			this.board.setFeature(initFeat);
			//this.board.printBoard();
			if (current.getNumBox() == 0) {
				
				this.board.printBoard();
				goalState = current;
				break;
			}
			
			//this.board.printBoard();

			ArrayList<String> possibleMove = this.board.getMovement();
			Iterator<String> itr = possibleMove.iterator();
			while (itr.hasNext()) {
				String move = itr.next();
				Position posChar = null;
				ArrayList<Position> features = null;
				//System.out.println(move);
				boolean seen = false;
				if (move.equals("left")) {
					this.board.moveLeft();
					posChar = this.board.getCloneChar();
					features = this.board.getFeaturePosition();
					
				} else if (move.equals("right")) {
					this.board.moveRight();
					posChar = this.board.getCloneChar();
					features = this.board.getFeaturePosition();
					
				} else if (move.equals("up")) {
					this.board.moveUp();
					posChar = this.board.getCloneChar();
					features = this.board.getFeaturePosition();
				} else if (move.equals("down")) {
					this.board.moveDown();
					posChar = this.board.getCloneChar();
					features = this.board.getFeaturePosition();
				}
				BoardState newState = new BoardState(posChar, features, current.getNumMove() + 1);
				//System.out.println(Integer.toString(current.getCharacter().getRow()) + " " + Integer.toString(current.getCharacter().getColumn()));

				this.board.clearBoard();
				this.board.setStartingLocation(charRow, charCol);
				this.board.setFeature(initFeat);
				Iterator<BoardState> bsItr = close.iterator();
				while (bsItr.hasNext()) {
					BoardState bs = bsItr.next();
					if (bs.isEqual(newState)) {
						seen = true;
						break;
					}
				}
				if (!seen) {
					int hValue = strat.getH(newState.getCharacter(), newState.getFeaturePosition(), newState.getNumMove());
					//System.out.println(Integer.toString(hValue));
					newState.setParent(current);
					newState.setMovement(move);
					newState.setHValue(hValue);
					open.add(newState);
				}
				if (Thread.currentThread().isInterrupted()) {
					return;
				}

			}
			
		}
		

		System.out.println("finish");	
	}
}
